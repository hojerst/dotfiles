local wezterm = require 'wezterm'
local config = wezterm.config_builder()

-----------------------------------------------------------------------------
-- Colors and Appearance
-----------------------------------------------------------------------------

config.font = wezterm.font 'MonoLisa'
config.font_size = 14.0

local colors = {
    bg = '#282C34',
    fg = '#888888',
    bg_dark = '#1B1F27',
    fg_dark = '#636873',
}

config.color_scheme = 'OneDark (base16)'
config.colors = {
    tab_bar = {
        active_tab = {
            bg_color = colors.bg,
            fg_color = colors.fg
        },

        inactive_tab = {
            bg_color = colors.bg_dark,
            fg_color = colors.fg_dark
        }
    }
}

config.use_fancy_tab_bar = true
config.window_frame = {
    active_titlebar_bg = colors.bg_dark,
    inactive_titlebar_bg = colors.bg_dark,
}

config.window_decorations = "INTEGRATED_BUTTONS|RESIZE"

-----------------------------------------------------------------------------
-- Keybindings
-----------------------------------------------------------------------------

config.keys = {
    {
        key = 'p',
        mods = 'CMD|SHIFT',
        action = wezterm.action.ActivateCommandPalette,
    },
    {
        key = 'k',
        mods = 'CMD',
        action = wezterm.action.ClearScrollback 'ScrollbackAndViewport',
    },
    {
        key = 'd',
        mods = 'CMD',
        action = wezterm.action.SplitVertical,
    },
    {
        key = 'd',
        mods = 'CMD|SHIFT',
        action = wezterm.action.SplitPane {
            direction = 'Left',
            size = { Percent = 50 },
            top_level = true,
        },
    },
}

-- and finally, return the configuration to wezterm
return config
