{ config, pkgs, lib, ... }:
let
  cfg = config.hojerst.dnsmasq;
in
{
  options = {
    hojerst.dnsmasq = {
      enable = lib.mkEnableOption "dnsmasq";
    };
  };

  config = lib.mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      dnsmasq
    ];

    services.dnsmasq.enable = true;
    services.dnsmasq.bind = "0.0.0.0";

    environment.etc = {
      "dnsmasq.conf".source = ./dnsmasq.conf;
    };
  };
}
