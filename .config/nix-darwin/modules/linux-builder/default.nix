{ config, pkgs, lib, ... }:
let
  cfg = config.hojerst.linux-builder;
in
{
  options = {
    hojerst.linux-builder = {
      enable = lib.mkEnableOption "linux-builder";
    };
  };

  config = lib.mkIf cfg.enable {
    nix.linux-builder = {
      enable = true;
      ephemeral = true;
      maxJobs = 4;
      config = {
        virtualisation = {
          darwin-builder = {
            diskSize = 40 * 1024;
            memorySize = 8 * 1024;
          };
          cores = 6;
        };
      };
    };
  };
}
