{ config, pkgs, ... }:
{
  hojerst = {
    dnsmasq.enable = true;
    linux-builder.enable = false;
  };

  nixpkgs.hostPlatform = "aarch64-darwin";
  nixpkgs.config.allowUnfree = true;

  nix = {
    extraOptions = ''
      extra-experimental-features = nix-command flakes
      extra-platforms = x86_64-darwin aarch64-darwin
    '';

    settings.trusted-users = ["@admin"];
  };

  environment.systemPackages = with pkgs; [
    bashInteractive
    coreutils-full
    gnused
    gnutar
  ];

  programs.zsh.enable = true;

  # Used for backwards compatibility, please read the changelog before changing.
  # $ darwin-rebuild changelog
  system.stateVersion = 4;
}
