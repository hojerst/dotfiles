# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
(( ${+commands[direnv]} )) && emulate zsh -c "$(direnv export zsh)"
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi
(( ${+commands[direnv]} )) && emulate zsh -c "$(direnv hook zsh)"

# register homebrew paths
foreach file (
        "/usr/local/bin/brew"(Nx)
        "/opt/homebrew/bin/brew"(Nx)
    )
    eval "$($file shellenv)"
end

# custom path
typeset -U path
path=(
    "$HOME/.local/bin"(N)
    "$HOME/.nix-profile/bin"(N)
    "$HOME/.krew/bin"(N)
    "$HOME/.rd/bin"(N)
    "$HOME/go/bin"(N)
    "${path[@]}"
)

umask 0022

# Uncomment the following line to use case-sensitive completion.
CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# omz plugins
plugins=(
    urltools
    extract
)

# custom fpath
fpath=(~/.config/zsh/functions ~/.config/zsh/completions $fpath)
autoload -U $fpath[1]/*(.:t)

# kept as a reminder - not needed because currently called by oh-my-zsh
#autoload -Uz compinit && compinit -i

# load all zsh config files
foreach file (
        # home-manager
        $HOME/.nix-profile/etc/profile.d/hm-session-vars.sh(N)
        # oh-my-zsh
        $ZDOTDIR/omz/oh-my-zsh.sh(N)
        # asdf
        $HOME/.asdf/asdf.sh(N)
        # zsh configs
        $ZDOTDIR/*.zsh(N)
        # zsh plugins
        $ZDOTDIR/plugins/*/*.plugin.zsh(N)
        # theme
        $ZDOTDIR/themes/powerlevel10k/powerlevel10k.zsh-theme(N)
        # highlight style
        $ZDOTDIR/highlights/classic.zsh(N)
        # command-not-found helper on debian/ubuntu
        /etc/zsh_command_not_found(N)
        # asdf direnv plugin
        ${XDG_CONFIG_HOME:-$HOME/.config}/asdf-direnv/zshrc(N)
    )
    source $file
end

export HISTFILE="$ZDOTDIR/.zsh_history"
