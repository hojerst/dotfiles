if (( $+commands[bat] )) ; then
    alias cat="bat"
    alias batcat="bat"
    export MANPAGER="bat -l man -p"
elif (( $+commands[batcat] )) ; then
    alias cat="batcat"
    alias bat="batcat"
    export MANPAGER="bat -l man -p"
fi
