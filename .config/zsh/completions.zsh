for i in hcloud kubectl velero kind flux k3d k9s gitlab-ci-docs; do
    if (( $+commands[$i] )) ; then
        source <($i completion zsh)
        compdef _$i $i
    fi
done

for i in glab; do
    if (( $+commands[$i] )) ; then
        source <($i completion -s zsh)
        compdef _$i $i
    fi
done

for i in oc ; do
    if (( $+commands[$i] )) ; then
        source <($i completion zsh)
    fi
done
