alias drun="docker run --rm -it -e TERM=$TERM"
if [ "$(uname -s)" = "Darwin" ] ; then
    alias druns="drun --mount type=bind,source=/run/host-services/ssh-auth.sock,target=/run/host-services/ssh-auth.sock -e SSH_AUTH_SOCK=/run/host-services/ssh-auth.sock"
else
    alias druns="drun --mount type=bind,source=${SSH_AUTH_SOCK},target=/run/host-services/ssh-auth.sock -e SSH_AUTH_SOCK=/run/host-services/ssh-auth.sock"
fi

alias ls='ls --color=tty'
alias drunw='drun -v "$PWD:/work" -w /work'
alias drunsw='druns -v "$PWD:/work" -w /work'
alias ip='ip --color=auto'
alias findrepos="find . -type d -name .git -exec dirname '{}' \\;"
alias k3d='KUBECONFIG="$HOME/.kube/config" k3d'
alias kubectl='kubecolor'
alias yadm='GIT_CONFIG_GLOBAL=$HOME/.config/git/config yadm'

alias aws='PYTHONPATH= aws'
