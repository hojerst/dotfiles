export FZF_DEFAULT_COMMAND="rg --files --hidden"

# find + edit file
fe() {
    if [ $# -ne 0 ] ; then
        # arguments: show only files with matching content
        rg -l "$@" | fzf -1 --preview="bat -p --color=always {}" | xargs -o "${EDITOR:-vim}"
    else
        # no arguments: show all files
        rg --files | fzf --preview="bat -p --color=always {}" | xargs -o "${EDITOR:-vim}"
    fi
}

# load fzf zsh integration if available
for i in /usr/{share/doc/fzf/examples,local/opt/fzf/shell}/*.zsh(N) ; do
    source $i
done
