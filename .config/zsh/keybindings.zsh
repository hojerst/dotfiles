bindkey '^[[H' beginning-of-line
bindkey '^[[F' end-of-line

# allow command line editing using $EDITOR
autoload -Uz edit-command-line
zle -N edit-command-line
bindkey ^V edit-command-line
