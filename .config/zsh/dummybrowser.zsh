if [ -z "$BROWSER" ] && ([ -n "$SSH_CONNECTION" ] || [ -e /.dockerenv ]) ; then
    export BROWSER=it2browser
fi
