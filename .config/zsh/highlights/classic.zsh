# syntax highlighting
ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets)

typeset -A ZSH_HIGHLIGHT_STYLES
ZSH_HIGHLIGHT_STYLES[default]="fg=cyan"
ZSH_HIGHLIGHT_STYLES[command]="bold"
ZSH_HIGHLIGHT_STYLES[precommand]="bold"
ZSH_HIGHLIGHT_STYLES[builtin]="bold"
ZSH_HIGHLIGHT_STYLES[function]="bold"
ZSH_HIGHLIGHT_STYLES[alias]="bold"
ZSH_HIGHLIGHT_STYLES[builtin]="bold"
ZSH_HIGHLIGHT_STYLES[comment]="fg=white"
ZSH_HIGHLIGHT_STYLES[single-hyphen-option]="fg=cyan"
ZSH_HIGHLIGHT_STYLES[double-hyphen-option]="fg=cyan"
ZSH_HIGHLIGHT_STYLES[path]="fg=blue"
ZSH_HIGHLIGHT_STYLES[path_prefix]="fg=blue,underline"
