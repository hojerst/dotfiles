{ config, pkgs, lib, ... }:
{
  home.packages = with pkgs; [
    _1password-cli
    argocd
    bat
    copier
    devbox
    devenv
    diff-so-fancy
    direnv
    docker
    docker-credential-helpers
    fzf
    git
    git-lfs
    git-open
    git-subrepo
    glab
    gomplate
    gopls
    hojerst.git-sync
    httpie
    ipcalc
    jq
    k9s
    kubecolor
    kubectl
    pre-commit
    pwgen
    ripgrep
    tmux
    tree
    vendir
    vim
    wakeonlan
    watch
    yq-go
  ] ++ lib.optionals pkgs.stdenv.isDarwin [
    colima
    m-cli
    mas
  ];

  services = {
    syncthing.enable = true;
  };

  programs = {
    home-manager.enable = true;
    direnv.enable = true;
    direnv.nix-direnv.enable = true;
  };
}
