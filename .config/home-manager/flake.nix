{
  description = "Home Manager configuration";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";
    nixpkgs-unstable.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

    hojerst-pkgs.url = "gitlab:hojerst/nix";
    hojerst-pkgs.inputs.nixpkgs.follows = "nixpkgs";

    home-manager.url = "github:nix-community/home-manager/release-24.11";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";

    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { flake-utils, hojerst-pkgs, nixpkgs, nixpkgs-unstable, home-manager, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        config.allowUnfree = true;
        pkgs = import nixpkgs {
          inherit overlays config system;
        };
        overlays = [
          (final: prev: {
            hojerst = hojerst-pkgs.packages.${final.system};
          })
          (final: prev: {
            inherit (nixpkgs-unstable.legacyPackages.${final.system}) git-open;
          })
        ];
      in
      {
        packages.homeConfigurations."hojerst" = home-manager.lib.homeManagerConfiguration {
          inherit pkgs;

          modules = [
            ./home.nix
            {
              home.username = "hojerst";
              home.homeDirectory = if pkgs.stdenv.isDarwin then "/Users/hojerst" else "/home/hojerst";
              home.stateVersion = "24.11";
            }
          ];
        };

        packages.homeConfigurations."root" = home-manager.lib.homeManagerConfiguration {
          pkgs = import nixpkgs {
            inherit overlays config system;
          };

          modules = [
            ./home.nix
            {
              home.username = "root";
              home.homeDirectory = "/root";
              home.stateVersion = "24.11";
            }
          ];
        };
      });
}
