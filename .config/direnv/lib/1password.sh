function 1password() {

  if [ $# -ne 1 ] ; then
      echo 2>&1 "usage: $0 accountname"
      return 1
  fi

  # op somehow always starts its daemon on linux leading this to hang, so we start a temp. daemon with 1 second
  # timeout ourself as a (clumsy) workaround
  op daemon -t1s &
  direnv_load op run --account="$1" --no-masking -- direnv dump
}
