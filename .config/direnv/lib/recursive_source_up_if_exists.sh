recursive_source_up_if_exists() {
    local dir="$(dirname "$(dirname "$(find_up .envrc)")")" parent stack=()
    while true ; do
        parent=$(cd "$dir" && find_up .envrc || true)
        if [ -f "$parent" ] && [ -r "$parent" ] ; then
            stack+=("$parent")
            dir=$(dirname "$(dirname "$parent")")
        else
            break
        fi
    done

    for (( i=${#stack[@]}-1 ; i>=0 ; i-- )) ; do
        source_env "${stack[i]}"
    done
}
