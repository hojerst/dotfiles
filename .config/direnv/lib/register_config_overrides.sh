register_config_overrides() {
    local config
    local base=.config

    if config="$(find_up $base/kube/config)" ; then
        for i in "$(dirname $config)"/* ; do
            if [ "$(basename $i)" != "config" ] ; then
                path_add KUBECONFIG "$i"
            fi
        done
        path_add KUBECONFIG "$HOME/.kube/config"
        path_add KUBECONFIG "$config"
    fi

    if config="$(find_up $base/helm/repositories.yaml)" ; then
        export HELM_CONFIG_HOME="$(dirname $config)"
    fi

    if config="$(find_up $base/git/config)" ; then
        export GIT_CONFIG_GLOBAL="$config"
    fi

    if config="$(find_up_dir $base/azure)" ; then
        export AZURE_CONFIG_DIR="$config"
    fi

    if config="$(find_up_dir $base/aws)" ; then
        export AWS_CONFIG_HOME="$config"
        export AWS_CONFIG_FILE="$config/config"
        export AWS_SHARED_CREDENTIALS_FILE="$config/credentials"
    fi

    if config="$(find_up $base/argocd/config)" ; then
        export ARGOCD_CONFIG="$config"
    fi

    if config="$(find_up_dir $base/bin)" ; then
        PATH_add "$config"
    fi
}
