use_devbox() {
    if [ $# -gt 0 ] ; then
        local template="$1"
        eval "$(devbox shellenv --init-hook --install --no-refresh-alias --config ~/.local/share/devbox/devboxes/$template)"
    else
        watch_file devbox.json devbox.lock
        eval "$(devbox shellenv --init-hook --install --no-refresh-alias)"
    fi
}
