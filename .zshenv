# explicitly set XDG dirs to defaults as some programs don't implement defaults
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"
export XDG_CACHE_HOME="$HOME/.cache"

# move zsh config to different folder
export ZDOTDIR="${XDG_CONFIG_HOME:-$HOME/.config}/zsh"

export EDITOR=vim
export KUBECTL_SHELL_IMAGE=hojerst/shell:latest

export KUBECTL_EXTERNAL_DIFF="diff --color"

export ANSIBLE_VAULT_PASSWORD_FILE="$HOME/.ansible/vault-pass"

if [ -n "${SSH_CONNECTION:-}${SSH_CLIENT:-}" ] ; then
    export OP_BIOMETRIC_UNLOCK_ENABLED=0
fi
