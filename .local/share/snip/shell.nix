{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShellNoCC {
  packages = with pkgs; [
    #git
  ];

  # Environment variables
  #EXAMPLE_VAR="value";

  # Shell customizations (aliases, functions) can be added here
  #shellHook = ''
  #  alias ll='ls -l'
  #'';
}

# vim: set tabstop=2:
